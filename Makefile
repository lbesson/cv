# Makefile for the cv git repository
# https://bitbucket.org/lbesson/cv/
#
# author: Lilian BESSON
# email: lilian DOT besson AT ens-cachan D O T fr
# version: 0.23
# date: 05/11/2018
##################################################
# Custom items
# CP = /usr/bin/rsync --verbose --times --perms --compress --human-readable --progress --archive
# CP = scp
CP = ~/bin/CP
GPG = gpg --detach-sign --armor --quiet --yes

# Default builder
quick:
	pdflatex cv.fr.tex
	pdflatex cv.fr.tex
	pdflatex cv.en.tex
	pdflatex cv.en.tex

small: cv.fr.pdf cv.en.pdf

stats:
	git-complete-stats.sh | tee complete-stats.txt

# Shortcuts
local: clean cv.fr.pdf cv.en.pdf concat clean evince

all: archive clean cv.fr.pdf cv.en.pdf concat evince git upload

cv: archive clean cv.fr.pdf cv.en.pdf concat git upload
lettre: archive clean lettre.pdf git upload

# Custom commands
cv.fr:	cv.fr.pdf
cv.en:	cv.en.pdf
cvfr:	cv.fr.tex
	pdflatex cv.fr.tex
	pdflatex cv.fr.tex
cven:	cv.en.tex
	pdflatex cv.en.tex
	pdflatex cv.en.tex

# Make one PDF with both CV (fr,en)
concat:	cv.fr.pdf cv.en.pdf
	pdftk cv.fr.pdf dump_data output /tmp/LBesson.pdf.meta
	pdftk cv.fr.pdf cv.en.pdf cat output LBesson.tmp.pdf
	PDFCompress LBesson.tmp.pdf
	pdftk LBesson.tmp.pdf update_info /tmp/LBesson.pdf.meta output LBesson.pdf
	$(GPG) LBesson.pdf
	-mv -vf LBesson.tmp.pdf /tmp/LBesson.tmp.pdf

# Custom builders for letters
cpge:	Lettre_motivation_CPGE__Lilian_Besson__2019.pdf

# Custom builders
git:
	git add cv*.tex Makefile README.md *.sty *.hva
	-git commit -m "Auto commit with 'make git'"
	git push

clean:
	-mv -vf ./*.aux ./*.haux ./*.htoc ./*.log ./*.out ./*.tmp.haux ./*.tmp.htoc ./*.tmp.txt ./*.synctex.gz ./*.frompdf* ./*.fls ./*.latex_mk ./*.fdb_latexmk /tmp/
	-rm -rf cv/ cv.en/ cv.fr/

cleanforce:
	-mv -vf ./*.pdf ./*.html ./*.txt ./*.asc ./*.meta /tmp/

evince: cv.fr.pdf cv.en.pdf
	evince cv.en.pdf cv.fr.pdf &>/dev/null&

evincelettres:
	evince lettre*.pdf &>/dev/null&

firefox: cv.fr.html cv.en.html
	firefox cv.en.html cv.fr.html &>/dev/null&

archive:
	zip -r -9 ~/Dropbox/cv.zip ./ > /tmp/cv.zip`date "+%d_%M__%H_%m_%S"`.log
	-$(GPG) ~/Dropbox/cv.zip

upload: send_public send_zamok

# send_dpt:
# 	-$(CP) cv*.pdf* lbesson@ssh.dptinfo.ens-cachan.fr:~/public_html/
# 	-$(CP) lettre*.pdf* lbesson@ssh.dptinfo.ens-cachan.fr:~/public_html/dl/.p/
# 	-$(CP) ~/Dropbox/cv*.tar.bz2 ~/Dropbox/cv*.tar.bz2.asc lbesson@ssh.dptinfo.ens-cachan.fr:~/public_html/dl/

send_zamok:
	-$(CP) cv*.pdf* besson@zamok.crans.org:~/www/
	-$(CP) Lettre*.pdf* lettre*.pdf* besson@zamok.crans.org:~/www/dl/.p/
	-$(CP) ~/Dropbox/cv*.tar.bz2 ~/Dropbox/cv*.tar.bz2.asc besson@zamok.crans.org:~/www/dl/

send_public:
	-$(CP) Lettre*.pdf* lettre*.pdf* cv*.asc cv*.pdf ~/Public/

# OLD OLD
meta = cv.meta

cv.hevea.html: cv.tex moderncv.hva
	hevea -O -fix $<
	-mv cv.html cv.hevea.html

cv.frompdf: cv.pdf
	-rm -f cv.frompdf* cv.frompdf*
	pdftohtml -p -s -enc "UTF-8" $< cv.frompdf.html
	-mv cv.frompdf-html.html cv.html

cv.version_html.html: cv.tex moderncv.hva
#	hevea -O -fix $<
	latex2html -font_size "10pt" -prefix cv.version_html -title "CV Lilian BESSON" -address "Lilian BESSON (lilian.besson[@]ens-cachan.fr)" -verbosity 4 -html_version "4.0,math,unicode" $<
# OLD OLD

.SUFFIXES:
.SUFFIXES: .tex .pdf .txt .html

.tex.pdf:
	+pdflatex $<
	pdflatex $<
	#-PDFCompress --sign $@

.pdf.html:
	pdftotext -layout -enc UTF-8 -eol unix -htmlmeta $<
	-$(GPG) $@

.pdf.txt:
	pdftotext -layout -enc UTF-8 -eol unix $<
	-$(GPG) $@
