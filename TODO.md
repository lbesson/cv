# TODO pour mon CV

## Présentation
- [x] J'aime bien ce violet moi
- [x] Maximum trois pages DONE
- [x] Uniformiser le style : dates, lieu, etc.
- [x] Ordre : enseignements, recherche + publis, activités annexes (orga, pédagogie, talks), code, formations, stages (suggère Simon)

## Compétences en informatique
- [x] A mettre mais à la fin du CV, pas là
- [x] Plus concis mais expliquer plus que je suis pro en Jupyter et Python et OCaml et tout ce qui est utilisé et requis pour la prépa et la prépa agrég
- [x] Une liste de langage : ajouter SQL

## Autres compétences
- [x] Actif sur GitHub et Bitbucket : ouais c'est un gros point,
- [x] On s'en fout des loisirs,
- [x] Sports : OK ?
- [x] Santé : on s'en fout,
- [x] Voyages : peut-être le garder et appuyer l'exotisme et la capacité d'adaptation ? (pour l'outre mer !)

## Diplômes
- [x] Pas parler du permis ni du bac : on s'en fout !

## Enseignement
- [x] Faire une somme du nombre d'heures totales ? (idée de Simon), Par exemple entre parenthèses après chaque endroit, écrire le total
- [x] Expliquer la mission à l'ENS Rennes : rapports de stage et soutenances orales, TPs de programmation, leçons et oraux de modélisation, oraux blancs, etc
- [x] Expliquer le cours à l'ENSAI, + lien vers le simulateur de Machine de Turing ?
- [x] Je dois valoriser plus les "vieilles" expériences : les corrections de copies et cours particuliers avant l'Inde ?! Pas sûr…

- [x] Valoriser mon année en Inde.

> Tu ne valorises pas assez ton passage en Inde. Le souvenir que j'ai de ta présentation à ton retour c'est qu'en gros tu étais le seul vraiment formé à comment enseigner les maths/info et que tu as aussi fourni un énorme travail dans les choix pédagogiques de l'équipe -- Simon.


## Éducation
- [x] Utiliser un meilleur style pour les années
- [x] Enlever les notes des licences, masters etc, laisser les rangs (en prépa, en agrég, au M2 MVA) et enlever le reste
- [x] XXX finalement non. Laisser les rangs d'entrées aux concours de prépa, je pense que c'est un bon point pour montrer que j'avais préparé tous les concours et pas que l'ENS ? Mais je laisse le fait d'avoir été accepté.
- [x] Bien plus concis sur la L3, M1, M2 ?
- [x] Pas dire Bachelor, Master
- [x] Pas besoin de détails des cours, juste rang et notes sur le MVA (c'est quand même un gros point)

## Stages de recherche
- [x] Ne même pas en parler à part le stage du MVA ? Ou juste une ligne sur le stage à Londres (pour l'expérience internationale et pas le sujet)

## Contacts de référence
- [x] Mettre collaborateurs enseignements et pas stages recherche
- [x] Mettre Didier Clouteau !
- [x] Mettre à jour : enlever avant l'Inde ?
- [x] Pas Florian de Vuyst, ni stage à Londres,
- [x] Pas de profs de l'ENS, mais plutôt les gens avec qui j'ai enseigné à Rennes (Romaric Gaudel, David Pichardie, David Cachera, Luc Bougé)

## Thèse
- [x] Je dois avoir un paragraphe résumant mon sujet de thèse, et le co-encadrement
- [x] Peut-être une rapide liste des séminaires et conférences où je suis allé en deux ans (il y a un certain nombre)

## Publications
- [x] Mettre une liste concise mais exhaustive de mes publications

## Autres activités
- [x] GouTP
- [x] Président de l'ADDI en 2017 : journée des doctorants !
- [x] Administrateur système des machines GNU/Linux de l'équipe SCEE, 2016-2019
- [x] DONE Organisation, gestion : ICT 2018  NOPE
- [x] DONE Responsabilités quelconque,  NOPE
- [x] Machins pédagogiques : PyCon.fr 2 fois, formations doctorales (j'en ai pas fait beaucoup en pédagogie)
- [x] Pour le crans ça dépend ce que tu y fais, si tu peux rattacher ça à de la pédagogie/vulgarisation c'est bien (genre faire des install parties)

- [x] Si j'ai de la place en 3ème page, m'en servir pour plus aérer le tout ?
- [x] Ou rajouter des choses : une rapide liste de projets en Python (ParcourSup.py, machine de Turing ?)

## Deuxièmes remarques de Simon
- [x] Pas de remarque sur ma mobilité ?
- [x] Pour le tout début j'aérerais un peu plus, par exemple pour pas mettre tout sur une meme ligne. Genre Doctorant séparé d'enseignant
- [x] Pour tes expériences d'enseignement c'est dense, il faudra essayer de fortement réfléchir à ce qui peut être supprimé. Le contenu est bon, mais réduire le nombre de caractère, buter les infos non pertinentes qui diluent
- [x] Les publis ça se compte pas, ça ne devrait pas se compter
- [x] Ah et sinon il reste la liste de références à retravailler.

---

# Ma lettre de motivation

- [x] Faire un plan de la lettre
- [x] Rédiger un premier jet, s'inspirant de celles de Damien, David, Pauline…
- [x] En faire une tartine sur les responsabilités administratives, la gestion de groupes, de tâches, de projets, etc
- [x] faire relire par F
- [x] faire relire par A
- [ ] faire relire par Lola : elle l'a pas relu finalement… dommage j'aurai bien aimé
- [x] Réduire en taille !
- [x] faire relire par F encore une fois


---

# Lettres de recommandation

- [x] David Pichardie : pour l'enseignement en prépa agrég
- [x] David Cachera : pour l'enseignement à l'ENSAI
- [x] Didier Clouteau : pour l'enseignement en Inde et les investissements sur place
- [x] Christophe Moy : pour la recherche, les activités parallèles, l'Inde et garantir que je vais soutenir avant l'été.
- [ ] Émilie Kaufmann : pour la recherche, le MVA peut-être et garantir que je vais soutenir avant l'été. TODO attendre fin lettre Christophe ?

---

# Mails

> Claudine me disait :
> Quand vous ferez votre démarche, écrivez aussi à Johan Yebbou (doyen) et Laurent Chéno (IG spécialiste de l'informatique) pour leur faire part de votre candidature.

> David me dit :
> Je te recommanderais bien, aussi, d’envoyer une lettre de candidature spontanée avec CV au proviseur de Bellepierre, et éventuellement de Leconte si tu veux : si de nouvelles classes sont créées, il pourrait y avoir un recrutement « local » hors-circuit

> David me dit aussi :
>  Si donc ça t’intéresse pour cette année ou la suivante, il te recommande de suivre la procédure que j’avais suivie, c’est-à-dire d’écrire aux IG et tout particulièrement à celle en charge de l’Académie de la Réunion (http://www.education.gouv.fr/cid118/index.html): Isabelle Moutoussamy, en te signalant comme candidat crédible ayant eu connaissance de l’éventuelle ouverture de nouvelles classes CPGE à La Réunion. Inutile de préciser le type et le lycée, d’après le Prov., d’autant qu’il y a des postes.

Isabelle.Moutoussamy <Isabelle.Moutoussamy AT Education.Gouv.fr>

http://www.education.gouv.fr/pid34309/mission-et-organisation-de-l-inspection-generale-de-l-education-nationale.html?ancre=mathematiques&pid=34309&ZONE_TEMPLATE_ID=8490&ZONE_TEMPLATE_MOD_ID=4087&ITEM_ID=9

Inspecteurs généraux Mathématiques <IG.Mathematiques AT Education.Gouv.Fr>

> Contacter aussi l'IG des académies de Rennes, de Lille, de Lyon, de Marseille directememnt !

> DONE Académie de Rennes (http://www.education.gouv.fr/cid117/index.html) : Michel Bovani
Michel Bovani <Michel.Bovani AT Education.Gouv.fr>
Isabelle Moutoussamy
Isabelle Moutoussamy <Isabelle.Moutoussamy@Education.Gouv.fr>

> NOPE Académie de Lille (http://www.education.gouv.fr/cid105/index.html) : Charles Torossian
Charles Torossian <Charles.Torossian AT Education.Gouv.fr>

> DONE Académie de Lyon (http://www.education.gouv.fr/cid107/index.html) :  Erick Roser
Erick Roser <Erick.Roser AT Education.Gouv.fr>

> DONE Académie de Marseille (http://www.education.gouv.fr/cid7/index.html) : Yohan Yebbou
Yohan Yebbou <Yohan.Yebbou AT Education.Gouv.fr>

**Doyen**

> DONE Académie de Grenoble (http://www.education.gouv.fr/cid102/index.html) : Laurent Chéno
Laurent Chéno <Laurent.Cheno AT Education.Gouv.fr>

**Doyen de l'option informatique**

> NOPE Académie de Toulouse (http://www.education.gouv.fr/cid123/index.html) : Olivier Sidokpohou
Olivier Sidokpohou <Olivier.Sidokpohou AT Education.Gouv.fr>

> NOPE Académie de Montpellier (http://www.education.gouv.fr/cid109/index.html) : Xavier Sorbe
Xavier Sorbe <Xavier.Sorbe AT Education.Gouv.fr>

---

# Prépas privées

> Voir la liste https://fr.wikipedia.org/wiki/Liste_des_classes_pr%C3%A9paratoires_aux_grandes_%C3%A9coles#La_R%C3%A9union

## À Rennes
### [Saint-Vincent](https://www.saintvincent-rennes.org/etablissement/organigramme/)
Contacts – CPGE
Secrétariat : Mme Soler – tél : 02.99.27.46.00
e-mail : sec-pedago@saintvincent-rennes.net

> Uniquement ECS (éco).

### [Assomption](http://cpge.assomption-rennes.org/index.php/renseignements)
02 99 36 31 76
secretariatprepa@assomption.bzh

> Uniquement PC & PSI.

## À Marseille ?

- Lycée privé Notre-Dame de Sion

## À Grenoble ou à côté ?

> Y'en a pas !

## À Lille ?

- Lycée privé Notre-Dame-de-la-Paix
- Lycée privé Saint-Paul
- Lycée privé Saint-Pierre

## À Nantes ?

- Lycée privé Saint-Stanislas

## À Toulouse ?

- Lycée privé Saint-Joseph

## À Lyon ?

- Lycée privé Aux Lazaristes
- Institution privée des Chartreux
- Lycée privé Notre-Dame des Minimes
- Externat privé Sainte-Marie

## À La Réunion ?

> Y'en a pas !

## À Montpellier ?

- Lycée privé Notre-Dame de la Merci 	Montpellier
- Lycée privé Emmanuel-d'Alzon 	Nîmes
